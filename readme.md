## "Potter CA" is a responsive CSS-only SVG animation using:

* Data-URI SVG [Mozilla](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs), [CanIUse](https://caniuse.com/#feat=datauri), [CSS Tricks](https://css-tricks.com/lodge/svg/09-svg-data-uris/)
* Clip-path [Mozilla](https://developer.mozilla.org/en-US/docs/Web/CSS/clip-path), [CanIUse](https://caniuse.com/#feat=css-clip-path)
* Custom Properies [Mozilla](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties), [CanIUse](https://caniuse.com/#feat=css-variables)
* Flexbox [Mozilla](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox), [CanIUse](https://caniuse.com/#feat=flexbox), [CSS Tricks](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
* Animation [Mozilla](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Animations/Using_CSS_animations), [CanIUse](https://caniuse.com/#feat=css-animation)
* Calc [Mozilla](https://developer.mozilla.org/en-US/docs/Web/CSS/calc), [CanIUse](https://caniuse.com/#feat=calc)
* Pseudo-elements [Mozilla](https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-elements), [CanIUse](https://caniuse.com/#search=Pseudo-elements)
* Codepen-style checkbox hack [CodePen](https://codepen.io/TimPietrusky/pen/fHmiI)
* Edit here: [JSFiddle](https://jsfiddle.net/dpotter05/3qxuLahf/)
* Author: David Potter
